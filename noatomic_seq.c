#include "constants.h"
#include <stdatomic.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

inline size_t fincr(size_t cnt) {
	for (size_t n = 0; n < NMAX; ++n) {
		++cnt;
	}
	return cnt;
}

int main(void) {
	size_t cnt = 0;
	for (size_t i = 0; i < NUM_THREADS; ++i) {
		cnt += fincr(0);
	}
	printf("seq The non-atomic counter is %zu\n", cnt);

	return EXIT_SUCCESS;
}
