#include <stdatomic.h>
#include <stdio.h>
#include <stdlib.h>
#include <threads.h>
#include <time.h>

#define NUM_THREADS 2U
 
static _Atomic unsigned int acnt = 0;
//static unsigned int cnt = 0;
 
static int fincr() {
	for(size_t i = 0; i < 1E9; ++i) {
//		++cnt;
		++acnt;
	}
	return 0;
}
 
int main(void) {
	clock_t tic =clock();

	thrd_t thr[NUM_THREADS];
	for(size_t i = 0; i < NUM_THREADS; ++i) {
		thrd_create(&thr[i], fincr, 0);
	}
	for(size_t i = 0; i < NUM_THREADS; ++i) {
		thrd_join(thr[i], 0);
	}

	printf("The atomic counter is %u\n", acnt);
//	printf("The non-atomic counter is %u\n", cnt);

	clock_t toc = clock();
	double exetime = difftime(toc, tic) / CLOCKS_PER_SEC;
	printf("Execution took %fs\n", exetime);

	return EXIT_SUCCESS;
}
