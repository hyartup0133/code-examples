#include <stdatomic.h>
#include <stdio.h>
#include <stdlib.h>
#include <threads.h>
#include <time.h>

#define NUM_THREADS 2U
 
static _Atomic unsigned int acnt = 0;
 
static int fincr() {
	unsigned int tcnt = 0;	
	for(size_t i = 0; i < 1E9; ++i) {
		++tcnt;
	}
	acnt += tcnt;

	return 0;
}
 
int main(void) {
	clock_t tic =clock();

	thrd_t thr[NUM_THREADS];
	for(size_t i = 0; i < NUM_THREADS; ++i) {
		thrd_create(&thr[i], fincr, 0);
	}
	for(size_t i = 0; i < NUM_THREADS; ++i) {
		thrd_join(thr[i], 0);
	}

	printf("The atomic counter is %u\n", acnt);

	clock_t toc = clock();
	double exetime = difftime(toc, tic) / CLOCKS_PER_SEC;
	printf("Function main finished in %fs.\n", exetime);

	return EXIT_SUCCESS;
}
