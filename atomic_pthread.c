#include <pthread.h>
#include <stdatomic.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define NUM_THREADS 2
 
static _Atomic unsigned int acnt = 0;
//static unsigned int cnt = 0;
 
static void* fincr() {
	for(size_t i = 0; i < 1E9; ++i) {
//		++cnt;
		++acnt;
	}
	return (void*)0;
}
 
int main(void) {
	clock_t tic =clock();

	pthread_t tid[NUM_THREADS];
	for (size_t i = 0; i < NUM_THREADS; ++i) {
		pthread_create(&tid[i], 0, fincr, 0);
	}
	for (size_t i = 0; i < NUM_THREADS; ++i) {
		pthread_join(tid[i], 0);
	}

	printf("The atomic counter is %u\n", acnt);
//	printf("The non-atomic counter is %u\n", cnt);

	clock_t toc = clock();
	double exetime = difftime(toc, tic) / CLOCKS_PER_SEC;
	printf("Execution took %fs\n", exetime);

	return EXIT_SUCCESS;
}
