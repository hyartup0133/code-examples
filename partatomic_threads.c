#include "constants.h"
#include <stdatomic.h>
#include <stdio.h>
#include <stdlib.h>
#include <threads.h>
#include <time.h>

int fincr(void* acnt) {
	size_t thr_cnt = 0;
	_Atomic size_t* p = acnt;
	for (size_t n = 0; n < NMAX; ++n) {
		++thr_cnt;
	}
	*p += thr_cnt;

	return 0;
}

int main(void) {
	size_t acnt = 0;
	thrd_t thr[NUM_THREADS];
	for (size_t n = 0; n < NUM_THREADS; ++n) {
		thrd_create(&thr[n], fincr, &acnt);
	}
	for (size_t n = 0; n < NUM_THREADS; ++n) {
		thrd_join(thr[n], 0);
	}
	printf("The atomic counter is %zu\n", acnt);

	return EXIT_SUCCESS;
}
