#include <stdint.h>
#include <stdio.h>      /* printf */
#include <stdlib.h>     /* malloc */
#include <sys/random.h> /* getrandom (syscall), ssize_t*/

int main(void) {
	int* random_int = malloc(sizeof(int));
	*random_int = 0;

	unsigned random_unsigned = 0;

	ssize_t retval_int = getrandom(random_int, sizeof random_int, 0);
	ssize_t retval_unsigned =
		getrandom(&random_unsigned, sizeof random_unsigned - 3, 0);

	/* 
	 * Print resulting number only if getrandom was successful, 
	 * otherwise it returns -1.
	 */
	if (retval_int < 0 || retval_unsigned < 0) {
	printf("getrandom returned (for int an uint): %zi, %zi\n", retval_int,
		retval_unsigned);
	} else {
	printf("random int: %i\n", *random_int);
	printf("random unsigned: %u\n", random_unsigned);
	}

	free(random_int);

	return EXIT_SUCCESS;
}
