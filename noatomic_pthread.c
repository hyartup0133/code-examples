#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define NUM_THREADS 2
 
//static unsigned int cnt;
static unsigned int nacnt[NUM_THREADS] = { 
	[0] = 0,
	[1] = 0,
};
 
static void* fincr(void *nArg) {
	for(int i = 0; i < 1E9; ++i) {
//		++cnt;
		++nacnt[(size_t)nArg];
	}
	return (void*)0;
}
 
int main(void) {
	clock_t tic =clock();

	pthread_t tid[NUM_THREADS];
	for (size_t i = 0; i < NUM_THREADS; ++i) {
		pthread_create(&tid[i], 0, fincr, (void*)i);
	}
	for (size_t i = 0; i < NUM_THREADS; ++i) {
		pthread_join(tid[i], 0);
	}

	unsigned int nacntsum = 0;
	for (size_t i = 0; i < NUM_THREADS; ++i) {
		nacntsum += nacnt[i];
	}

	printf("The non-atomic counter is %u\n", nacntsum);
//	printf("The non-atomic counter is %u\n", cnt);

	clock_t toc = clock();
	double exetime = difftime(toc, tic) / CLOCKS_PER_SEC;
	printf("Execution took %fs\n", exetime);

	return EXIT_SUCCESS;
}
