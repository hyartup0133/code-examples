#include "constants.h"
#include <stdio.h>
#include <stdlib.h>
#include <threads.h>

static size_t cnt = 0;

static int fincr() {
	for (size_t i = 0; i < NMAX; ++i) {
		++cnt;
	}
	return 0;
}
int main(void) {
	thrd_t thr[NUM_THREADS];
	for (size_t i = 0; i < NUM_THREADS; ++i) {
		thrd_create(&thr[i], fincr, 0);
	}
	for (size_t i = 0; i < NUM_THREADS; ++i) {
		thrd_join(thr[i], 0);
	}

	printf("flawed non-atomic counter is %zu\n", cnt);

	return EXIT_SUCCESS;
}
